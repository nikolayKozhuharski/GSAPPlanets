import gsap from "gsap/all";

export default class Animation {
  constructor() {
    this._planets = document.getElementsByClassName("dots");
    this._scaleBtn = document.querySelector("#scale-button");
    this._positionBtn = document.querySelector("#position-button");
    this._stopBtn = document.querySelector("#stop-button");
    this._tl = gsap.timeline();
    this.init();
  }
  async init() {
    this._scaleBtn.addEventListener("click", () => {
      this._tl.restart();
      this._tl.clear();
      this._tl.to(this._planets, {
        scale: 0,
        repeat: -1,
        duration: 0.5,
        stagger: 0.1,
        yoyo: true,
        id: "scaleStagger",
      });
    });
    this._positionBtn.addEventListener("click", () => {
      this._tl.restart();
      this._tl.clear();
      this._tl.to(this._planets, {
        y: -100,
        stagger: {
          amount: 1,
          edges: true,
        },
        repeat: -1,
        yoyo: true,
        id: "positionStagger",
      });
    });

    this._stopBtn.addEventListener("click", () => {
      this._tl.restart()
      this._tl.clear();
    });
  }
}
